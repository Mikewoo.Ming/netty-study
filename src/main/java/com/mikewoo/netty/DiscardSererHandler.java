package com.mikewoo.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

public class DiscardSererHandler extends ChannelHandlerAdapter {

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.close();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf buf = (ByteBuf) msg;
		while (buf.isReadable()) {
			System.out.print((char)buf.readByte());
			System.out.flush();
		}
		
		ByteBuf wbuf = ctx.alloc().buffer().writeBytes("hello I have saved your message".getBytes("UTF-8"));
		ctx.writeAndFlush(wbuf);
	}

	
}
