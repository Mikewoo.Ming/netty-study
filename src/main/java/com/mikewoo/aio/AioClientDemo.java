package com.mikewoo.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.Charset;
import java.util.concurrent.Future;

/**
 * Title: AioClientDemo.java
 * Description: 利用AIO实现的客户端 
 * 
 * author     Phantom
 * date       2016年5月20日 上午10:34:48
 * @version    1.0.0
 */
public class AioClientDemo {
	public static void startClient() throws Exception {
		AsynchronousSocketChannel asynchronousSocketChannel = AsynchronousSocketChannel.open();
		Future<?> future = asynchronousSocketChannel.connect(new InetSocketAddress("localhost", 8999));
		future.get(); 
		ByteBuffer buffer = ByteBuffer.wrap("hello aio server".getBytes("UTF-8"));
		asynchronousSocketChannel.write(buffer);
		buffer.clear();
		asynchronousSocketChannel.close();
	}
	
	public static void main(String[] args) throws Exception {
		startClient();
	}
}
